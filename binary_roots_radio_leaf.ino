/**

  Reads commands like ping and status, and responds to them.
  No AutoACK

  Switch (relay) added.

  Developed using TMRh20's implementation of RF24 library.
  https://github.com/TMRh20/RF24
*/

#include <RF24_config.h>
//#include <printf.h>
#include <nRF24L01.h>
#include <RF24.h> // TMRh20


#define RADIO_XCS           8
#define RADIO_CE            7
#define SWITCH_PIN          6
#define RADIO_CHANNEL       121
#define RADIO_PAYLOAD_SIZE  32
#define SERIAL_SPEED        250000
#define AUTO_ACK_ENABLED    false

#define MASTER_ADDR         0x63C1FA6300LL
#define THIS_DEVICE_ADDR    0x63C1FA6301LL

RF24 radio(RADIO_CE, RADIO_XCS);

uint8_t addr = 0;


//
// > ByteBuffer class starts
//
class ByteBuffer {
  public:
    void     write(uint8_t octet);
    void     clean();
    uint8_t  buff[32];
    volatile uint8_t size = 0;
    //  private:
};

void ByteBuffer::write(uint8_t octet) {
  buff[size] = octet;
  if (size < 31) { // TODO put block here
    size++;
  }
}

void ByteBuffer::clean() {
  size = 0;
}
//
// < ByteBuffer class ends
//

//
// > PipeBuffer class starts
//
/*
  The state of pointers the same for the empty and full buffers. Check if
  it can be resolved somehow (for ex., using an additional boolean flag
  of fullness. One byte is lost otherwise.
*/
class PipeBuffer {
  public:
    PipeBuffer(uint8_t byteArray[], uint8_t len);
    uint8_t   available();
    void      writeByte(int8_t b);
    uint8_t   readByte();
  private:
    uint8_t*  buff;
    uint8_t   len;
    uint8_t   s = 0; // Start. Points to the last readed byte
    uint8_t   e = 0; // End. Points to the last written (unread) byte
};

uint8_t PipeBuffer::available() {
  if (s <= e) {
    return e - s;
  } else {
    return e + len - s;
  }
}

PipeBuffer::PipeBuffer(uint8_t byteArray[], uint8_t length) {
  buff = byteArray;
  len = length;
}


void PipeBuffer::writeByte(int8_t b) {
  if (++e == len) {
    e = 0;
  }
  buff[e] = b;
}

uint8_t PipeBuffer::readByte() {
  if (++s == len) {
    s = 0;
  }
  return buff[s];
}
//
// < PipeBuffer class ends
//

#define RADIO_BUFF_LEN 33

uint8_t rBuffArray[RADIO_BUFF_LEN];
PipeBuffer rBuf = PipeBuffer(rBuffArray, RADIO_BUFF_LEN);

ByteBuffer byteBuffOut = ByteBuffer();

//
// Stream i/o definitions start
//
void writeByteStream(uint8_t octet) {
  byteBuffOut.write(octet);
}

uint8_t readByteStream() {
  return rBuf.readByte();
}

boolean isAvailableStream() {
  return rBuf.available();
}

void transmitPacket(ByteBuffer &byteBuff) {

  radio.stopListening();
  radio.write(byteBuff.buff, byteBuff.size);
  radio.startListening();
}
//
// Stream definitions end
//


//
// > Nodes definition starts ----------------------------------------------
//
#define HEADER_END         0x80

#define NODE_PING          0x01
#define NODE_PONG          0x02
#define NODE_PACKET_NUM    0x03
#define NODE_DATA_REQ      0x04
#define NODE_STATUS        0x05
#define NODE_MILLIS        0x06
#define NODE_STATUS_CHANGE 0x07
#define NODE_LED           0x08
#define NODE_SWITCH        0x09
#define NODE_ENVELOPE      0x11
#define NODE_TO            0x12
#define NODE_FROM          0x13
#define NODE_DATA          0x14
//
// < Nodes definition ends
//


//
// > RootReader class starts
//
class RootReader {
  public:
    uint32_t readUint32(boolean &hasData);
    uint8_t  readUint7();
    uint16_t readHeader();
    void     skipRoot();
    uint16_t timeOutCycles = 65535; // Tries before reading byte fails
    uint8_t  readByte();
  private:
    uint8_t  lastByte;
    boolean  isPushedBack = false;

    uint16_t readType(byte firstByte);
    void     pushBack(uint8_t byte);
};

uint8_t RootReader::readByte() {
  uint8_t byte;
  if (isPushedBack) {
    byte = lastByte;
    isPushedBack = false;
  } else {
    uint8_t timeOut = timeOutCycles;
    while (!isAvailableStream() && --timeOut > 0);
    if (timeOut != 0) {
      byte = readByteStream();
    } else {
      byte = 0x80;
    }
  }
  return byte;
}

void RootReader::pushBack(uint8_t byte) {
  isPushedBack = true;
  lastByte = byte;
}

uint16_t RootReader::readType(byte firstByte) {
  uint16_t result = firstByte & 0x3f;
  if (!(firstByte & 0x40)) {
    return result;
  }
  uint8_t shift = 6;
  do {
    firstByte = readByte();
    uint16_t temp = firstByte & 0x3f;
    result = result + (temp << shift);
    shift += 6;
  } while (firstByte & 0x40);
  return result;
}

uint16_t RootReader::readHeader() {

  // Wait for header
  uint8_t octet;
  while ((octet = readByte()) < 0x80) {}

  return readType(octet);
}

/**
  This method is used for speed up purpose, instead of resource-cost 'readUint32'.
*/
uint8_t RootReader::readUint7() {

  uint8_t result = readByte();

  if (result >= 0x80) {
    pushBack(result);
  }
  return result;
}

uint32_t RootReader::readUint32(boolean &hasData) {

  uint32_t result = 0x00;
  uint8_t  shift = 0;
  uint32_t octet;

  hasData = false;
  while ((octet = readByte()) < 0x80) {
    hasData = true;
    octet = octet << shift;
    result |= octet;
    shift += 7;
  }
  pushBack(octet);

  return result;
}

void RootReader::skipRoot() {
  uint8_t octet;
  uint8_t deep = 0;

  do {
    octet = readByte();
    if (octet == 0x80) {
      if (deep == 0) {
        return;
      } else {
        deep--;
      }
    } else if (octet > 0x80) {
      deep++;
    }
  } while (true);


  while (deep > 0) {
    octet = readByte();
    if (octet > 0x80) {
      deep++;
    } else if (octet == 0x80) {
      deep--;
    }
  }
}
//
// RootReader class ends
//


//
// > RootWriter class starts
//
class RootWriter {
  public:
    void sendHeader(uint8_t nodeType);
    void sendEnd();
    void sendUint32(uint32_t data);
    void sendUint7(uint8_t data);
    void sendTextAscii(const char text[]);
    void sendSimpleNode(uint8_t nodeType);
  private:
    void sendByte(uint8_t byte);
};

void RootWriter::sendByte(uint8_t byte) {
  writeByteStream(byte);
}

void RootWriter::sendHeader(uint8_t type) {
  sendByte(0x80 | (type & 0x3f));
}

void RootWriter::sendEnd() {
  sendByte(HEADER_END);
}

void RootWriter::sendUint7(uint8_t data) {

  sendByte(data & 0x7f);
}

void RootWriter::sendTextAscii(const char text[]) {

  for (uint8_t i = 0; i < sizeof(text) - 1; i++) {
    sendUint7(text[i]);
  }
}

void RootWriter::sendUint32(uint32_t data) {
  do {
    uint8_t toSend = data & 0x7f;
    sendByte(toSend);
    data >>= 7;
  } while (data > 0);
}

void RootWriter::sendSimpleNode(uint8_t nodeType) {
  sendHeader(nodeType);
  sendEnd();
}

//
// < RootWriter class ends --------------------------------------------------
//

//
// Program part starts
//
RootReader rr = RootReader();
RootWriter rw = RootWriter();

void sendNumberNode(uint32_t nodeType, uint32_t number){
  rw.sendHeader(nodeType);
  rw.sendUint32(number);
  rw.sendEnd();
}


uint8_t switchPin = SWITCH_PIN;
uint32_t statusChangeMillis = 0;

void sendNodeMillis() {
  rw.sendHeader(NODE_MILLIS);
  uint32_t timeMillis = millis();
  rw.sendUint32(timeMillis);
  rw.sendEnd();
}

void sendNodeStatusChangeMillis() {
  rw.sendHeader(NODE_STATUS_CHANGE);
  rw.sendUint32(millis() - statusChangeMillis);
  rw.sendEnd();
}


void sendNodeLed() {
  rw.sendHeader(NODE_LED);
  if (digitalRead(LED_BUILTIN)) {
    rw.sendUint7(HIGH);
  } else {
    rw.sendUint7(LOW);
  }
  rw.sendEnd();
}


void sendNodeStatus() {
  rw.sendHeader(NODE_STATUS);
  sendNodeLed();
  sendNodeMillis();
  sendNodeStatusChangeMillis();
  sendNodeSwitch();
  rw.sendEnd();
}

void readNodeStatus() {
  sendNodeStatus();
  rr.skipRoot();
}

void readNodePing() {
  byteBuffOut.clean();
  boolean hasDataIgnored;
  uint32_t pingNum = rr.readUint32(hasDataIgnored);
  sendNumberNode(NODE_PONG, pingNum);
  transmitPacket(byteBuffOut);
  rr.skipRoot();
  Serial.println("oPong.");
}


void sendNodeSwitch() {
  rw.sendHeader(NODE_SWITCH);
  if (!digitalRead(switchPin)) { // Inversed
    rw.sendUint7(HIGH);
  } else {
    rw.sendUint7(LOW);
  }
  rw.sendEnd();
}


void readNodeSwitch() {
  byteBuffOut.clean();
  boolean hasDataIgnored;
  uint32_t pingNum = rr.readUint32(hasDataIgnored);
  if (pingNum & 0x01){
    digitalWrite(switchPin, HIGH);
  } else {
    digitalWrite(switchPin, LOW);
  }
  sendNodeSwitch();
  rr.skipRoot();
  transmitPacket(byteBuffOut);
}

void readMasterPacket() {
  uint16_t type;
  while ((type = rr.readHeader()) != 0) {
    switch (type) {
      case NODE_PING:
        Serial.println("got ping");
        readNodePing();
        break;
      case NODE_STATUS:
        Serial.println("got status");
        readNodeStatus();
        break;
      case NODE_SWITCH:
        readNodeSwitch();
        break;
      default:
        Serial.println("got unknown");
        rr.skipRoot();
    }
  }
}


void setupRadio() {

  radio.begin();

  radio.enableDynamicPayloads();
  radio.setChannel(RADIO_CHANNEL);
  radio.setAutoAck(AUTO_ACK_ENABLED);

  radio.openWritingPipe(MASTER_ADDR); // Writing to Master
  radio.openReadingPipe(1, THIS_DEVICE_ADDR); // Listening on our addr

  radio.startListening();

  radio.printDetails();
}


void setupRelay(){
  
    pinMode(switchPin, OUTPUT);
}



uint32_t packetCounter = 0;
byte radioReadBuf[32];


void setup() {

  Serial.begin(SERIAL_SPEED);
//  printf_begin();
  Serial.println("Leaf Initialization...");
  setupRadio();
  setupRelay();
  Serial.println("Initialization done.");
}

/*
  void printRBuf() {
  Serial.print(" rBuf: [");
  while (rBuf.available()) {
    uint8_t octet = rBuf.readByte();
    Serial.print(octet);
    Serial.print(' ');
  }
  Serial.println("]");
  }*/


void loadToPipe(byte* data, uint8_t len) {
  Serial.print(" data: [");
  for (uint8_t i = 0; i < len; ++i) {
    rBuf.writeByte(data[i]);
    Serial.print(data[i]);
    Serial.print(' ');
  }
  Serial.println("]");
  //printRBuf();
}


boolean tx_ok;
boolean tx_fail;
boolean rx_ready;

#define ACTIVITY_COUNTER_CYC 10000
uint16_t activityCounter   = ACTIVITY_COUNTER_CYC;
uint8_t  lineCounter       = 30;
uint8_t  fullStatusCounter = 10;

void showActivity() {
  if (--activityCounter == 0) {
    activityCounter = ACTIVITY_COUNTER_CYC;
    if (--lineCounter == 0) {
      lineCounter = 30;
      Serial.println();
    }
    char toPrint;
    if (radio.rxFifoFull()) {
      toPrint = 'F';
    } else {
      toPrint = '.';
    }
    Serial.print(toPrint);
  }
}

void loop() {

  // Check radio stream
  if (radio.available()) {
    activityCounter = ACTIVITY_COUNTER_CYC;
    packetCounter++;
    Serial.print("iGot packet N ");
    Serial.print(packetCounter);
    uint8_t len = radio.getDynamicPayloadSize();
    if (len > 0) { // No need to read zero packet
      Serial.print(" (");
      Serial.print(len);
      Serial.println(" bytes)");
      radio.read(&radioReadBuf, len);
      loadToPipe(radioReadBuf, len);
    }
  }
  //   Process buffer
  if (isAvailableStream()) {
    activityCounter = ACTIVITY_COUNTER_CYC;
    Serial.print("Reading... ");
    readMasterPacket();
  }
  showActivity();
}



